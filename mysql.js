//========RECORDAR CORRER node .\mysql.js EN TERMINAL PARA EJECUTAR===========
const mysql = require("mysql");

//----------------PROCEDIMIENTO CON ESTRUCTURA CREADA-------------------------
// var db = mysql.createConnection({
//     host: "localhost",
//     user: "Carlos",         //CAMBIAR POR TU USUARIO
//     password: "carlos",     //CAMBIAR POR TU CONTRASEÑA
//     database: "usuarios",
// });

// db.connect((err) => {
//     if (err) throw err; //SI EXISTE ERROR, LANZAMOS ERROR
//     console.log("Conexión a la base de datos 'usuarios', correcta!");
// });

// let id, name, pass, level;

// db.query("SELECT * FROM users", (err, rows) => {
//     if (err) throw err;
//     console.log("El objeto resultante del query SELECT es: ");
//     console.log(rows);
// });

// name = "Mauro";
// pass = "mauro";
// level = 5;

// db.query(
//     `INSERT INTO users (nombre, password, nivel) VALUES ("${name}","${pass}",${level})`,
//     (err, rows) => {
//         if (err) {
//             console.log("Error capturado: ");
//             throw err;
//         }
//         console.log(rows);
//         console.log(rows.affectedRows);
//         console.log(rows.insertId);
//     }
// );

// db.query(
//     `INSERT INTO users (nombre, password, nivel) VALUES ("Julieta","julieta",2), ("Romina","romina",2)`,
//     (err, rows) => {
//         if (err) {
//             console.log("Error capturado: ");
//             throw err;
//         }
//         console.log(rows);
//         console.log(rows.affectedRows);
//         console.log(rows.insertId);
//     }
// );

// db.query("SELECT * FROM users", (err, rows) => {
//     if (err) throw err;
//     console.log("El objeto resultante del query SELECT es: ");
//     console.log(rows);
// });

// name = "Julieta";
// level = 4;
// db.query(
//     `UPDATE users SET nivel = ${level} WHERE nombre = "${name}"`,
//     (err, rows) => {
//         if (err) throw err;
//         console.log("RESULTADO DEL UPDATE");
//         console.log(rows);
//         console.log(rows.affectedRows);
//     }
// );

// id = 4;
// name = "Romina";
// db.query(`DELETE FROM users WHERE nombre = "${name}"`, (err, rows) => {
//     if (err) throw err;
//     console.log("RESULTADO DEL DELETE");
//     console.log(rows);
//     console.log(rows.affectedRows);
// });

// level = 4;
// db.query(`SELECT * FROM users WHERE nivel > ${level}`, (err, rows) => {
//     if (err) throw err;
//     console.log("RESULTADO DE LA LECTURA SELECTIVA POR NOMBRE");
//     console.log(rows);
//     console.log(rows.length);
//     console.log(rows[0].nombre);
// });

// db.end();
//----------------------FIN PROCEDIMIENTO CON ESTRUCTURA CREADA---------------------

//----------------------PROCEDIMIENTO BORRADO --------------------------------------
// var db = mysql.createConnection({
//     host: "localhost",
//     user: "Carlos",
//     password: "carlos",
//     database: "usuarios"
// });

// db.query("DROP TABLE IF EXISTS users", (err, rows) => {
//     if (err) throw err;
//     console.log("Se eliminó la tabla 'users' satisfactoriamente!");
//     console.log(rows);
// })

// db.query("DROP DATABASE IF EXISTS usuarios", (err, rows) => {
//     if (err) throw err;
//     console.log("Se eliminó la base de datos 'usuarios' satisfactoriamente!");
//     console.log(rows);
// })

// db.end();
//---------------------------FIN PROCEDIMIENTO BORRADO------------------------------

//---------------------------PROCEDIMIENTO CREACIÓN DE ESTRUCTURA-------------------
var db = mysql.createConnection({
    host: "localhost",
    user: "Carlos",
    password: "carlos",
    //database: "usuarios"
});

db.query("CREATE DATABASE IF NOT EXISTS usuarios", function (err, rows) {
    if (err) throw err;
    console.log("Database Created..!!");
    console.log(rows);
});

db.query(`CREATE TABLE IF NOT EXISTS usuarios.users (id INT NOT NULL AUTO_INCREMENT, nombre VARCHAR(30) NOT NULL, password VARCHAR(20) NOT NULL, nivel INT NOT NULL, PRIMARY KEY (id)) ENGINE = InnoDB`, function (err, rows){
    if (err) throw err;
    console.log("Tabla users created...!!");
    console.log(rows);
})

db.end();
//-----------------------------FIN PROCEDIMIENTO CREACIÓN ESTRUCTURA---------------------
//-----------------------------PROCEDIMIENTO CARGA DE INFO-------------------------------
// var db = mysql.createConnection({
//     host: "localhost",
//     user: "Carlos",
//     password: "carlos",
//     database: "usuarios"
// });

// db.connect((err) => {
//     if (err) throw err; //SI EXISTE ERROR, LANZAMOS ERROR
//     console.log("Conexión a la base de datos 'usuarios', correcta!");
// });

// db.query(
//     `INSERT INTO users (nombre, password, nivel) VALUES ("Carlos","carlos", 7), ("Mariana","mariana", 5), ("Daniela","daniela", 4)`,
//     (err, rows) => {
//         if (err) {
//             console.log("Error capturado: ");
//             throw err;
//         }
//         console.log(rows);
//         console.log(rows.affectedRows);
//         console.log(rows.insertId);
//     }
// );

// db.end();
//--------------------------FIN PROCEDIENTO CARGA INFO-------------------------
